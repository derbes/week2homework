package week2homework

func Bsort(arr *[]int) []int {
	// bubble sort
	// time complexity O(n*n)
	for i:=0;i<len(*arr); i++{
		for j:=0;j<len(*arr);j++{
			if (*arr)[j]>(*arr)[i] {
				(*arr)[i], (*arr)[j] = (*arr)[j], (*arr)[i]
			}
		}
	}
	return (*arr)
}


